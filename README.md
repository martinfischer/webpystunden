# webpystunden
webpystunden ist eine Django Webapp zum Aufzeichnen von Stunden und zur automatischen
PDF Rechnungs Erstellung. Besonders geeignet für IT Freelancer, Selbstständige
und kleinere Betriebe.

## Features

* Erstellung von Stundenaufzeichnungen, Firmen und Arbeitnehmern
* Erstellung von PDF Rechnungen mit eingebetteter Stundentabelle
* CSV Daten Export
* CSV Daten Import
* JSON API
* Tests vorhanden

## Verwendete Technologien

* Python
* Django
* Reportlab
* Tastypie
* jQuery
* Bootstrap

## Installation

In einem Linux Terminal mit Python 2.7.x, virtualenv und git installiert:
```
cd workspace
virtualenv webpystunden

cd webpystunden
source bin/activate

git clone https://github.com/martinfischer/webpystunden.git

cd webpystunden
pip install -r requirements.txt

mv webpystunden/example-settings.py webpystunden/settings.py
gedit webpystunden/settings.py
(Folgende Einstellungen nach Geschmack ändern: ADMINS und SECRET_KEY)

python manage.py syncdb (Hier einen Superuser erstellen!)
python manage.py migrate

gedit stunden/models.py
(Zeile 12 entkommentieren)

python manage.py test stunden
python manage.py runserver
```

## Lizenz

MIT
