# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url


urlpatterns = patterns(
    "",
    # Die URls für die stunden app.
    url(r"^", include("stunden.urls")),
)
