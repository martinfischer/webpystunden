# -*- coding: utf-8 -*-
from .models import StundenAufzeichnung, Firma, Arbeitnehmer
from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field
from crispy_forms.bootstrap import AppendedText
from datetime import date
from dateutil.relativedelta import relativedelta


class FirmaForm(forms.ModelForm):
    """
    Das Formular für einen neuen Firma Eintrag.
    """

    firma = forms.CharField(
        error_messages={
            "unique": u"Firma mit diesem Firmen Namen existiert bereits."}
    )

    class Meta:
        model = Firma

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.help_text_inline = True
        self.helper.layout = Layout(
            Field("firma", css_class="span4"),
            Field("name", css_class="span4"),
            Field("adresse", css_class="span4"),
            Field("plz", css_class="span4"),
            Field("ort", css_class="span4"),
            Field("land", css_class="span4"),
            Field("uid", css_class="span4"),
        )
        super(FirmaForm, self).__init__(*args, **kwargs)


class ArbeitnehmerForm(forms.ModelForm):
    """
    Das Formular für einen neuen Arbeitnehmer Eintrag.
    """
    class Meta:
        model = Arbeitnehmer

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.help_text_inline = True
        self.helper.layout = Layout(
            Field("name", css_class="span4"),
            Field("adresse", css_class="span4"),
            Field("plz", css_class="span4"),
            Field("ort", css_class="span4"),
            Field("land", css_class="span4"),
            Field("uid", css_class="span4"),
            Field("bank_name", css_class="span4"),
            Field("bank_iban", css_class="span4"),
            Field("bank_bic", css_class="span4"),
        )
        super(ArbeitnehmerForm, self).__init__(*args, **kwargs)


class StundenAufzeichnungForm(forms.ModelForm):
    """
    Das Formular für einen neuen Stunden Eintrag.
    """
    datum = forms.DateField(
        initial="{}".format(date.today().strftime("%Y-%m-%d")),
        help_text="(Format: yyyy-mm-tt)",
    )

    button_firma = """
    <a href="/firma/neu/" class="btn">
    <i class="icon-plus"></i></a>
    """

    firma = forms.ModelChoiceField(
        label="Firma",
        queryset=Firma.objects.all(),
        help_text=button_firma,
        required=True,
    )

    startzeit = forms.TimeField(
        initial="08:00",
        help_text="(Format: 5:30, 15:30 oder 17:30:00)",
    )

    endzeit = forms.TimeField(
        initial="15:30",
        help_text="(Format: 5:30, 15:30 oder 17:30:00)",
    )

    button_arbeitnehmer = """
    <a href="/arbeitnehmer/neu/" class="btn">
    <i class="icon-plus"></i></a>
    """

    arbeitnehmer = forms.ModelChoiceField(
        label="Arbeitnehmer",
        queryset=Arbeitnehmer.objects.all(),
        help_text=button_arbeitnehmer,
        required=True,
    )

    class Meta:
        model = StundenAufzeichnung

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.help_text_inline = True
        super(StundenAufzeichnungForm, self).__init__(*args, **kwargs)


class RechnungsForm(forms.Form):
    """
    Das Formular für die Rechnung.
    """
    last_month = date.today() + relativedelta(months=-1)
    last_month_formated = last_month.strftime("%m-%Y")

    firma = forms.ModelChoiceField(
        label="Rechnungsempfänger",
        queryset=Firma.objects.all(),
        required=True,
    )

    rechnungs_nummer = forms.CharField(
        label="Rechnungsnummer",
        max_length=50,
        help_text="(Die Rechnungsnummer benennt auch das PDF)",
        required=True,
    )

    rechnungs_titel = forms.CharField(
        label="Rechnungstitel",
        max_length=100,
        initial="Programmierungsdienste {}".format(last_month_formated),
        help_text="(Ein Titel mit letztem Monat wurde automatisch "
                  "eingetragen)",
        required=True,
    )

    rechnungs_stundenlohn = forms.DecimalField(
        label="Stundenlohn",
        help_text="(Zulässig sind Werte wie: 50, 50.5 oder 50,55)",
        localize=True,
        max_digits=5,
        decimal_places=2,
        required=True,
    )

    meine_daten = forms.ModelChoiceField(
        label="Arbeitnehmer",
        queryset=Arbeitnehmer.objects.all(),
        required=True,
    )

    def __init__(self, *args, **kwargs):
        """
        Fügt Feldern CSS und Bootstrap Styling hinzu.
        """
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.help_text_inline = True
        self.helper.layout = Layout(
            Field("firma", css_class="span4"),
            Field("rechnungs_nummer",
                  css_class="span4"),
            Field("rechnungs_titel",
                  css_class="span4"),
            Field(AppendedText("rechnungs_stundenlohn", "€",
                  css_class="span4",
                               )),
            Field("meine_daten", css_class="span4"),
        )
        super(RechnungsForm, self).__init__(*args, **kwargs)


class RechnungsSummeForm(forms.Form):
    """
    Das Formular für die Rechnung.
    """
    last_month = date.today() + relativedelta(months=-1)
    last_month_formated = last_month.strftime("%m-%Y")

    firma = forms.ModelChoiceField(
        label="Rechnungsempfänger",
        queryset=Firma.objects.all(),
        required=True,
    )

    rechnungs_nummer = forms.CharField(
        label="Rechnungsnummer",
        max_length=50,
        help_text="(Die Rechnungsnummer benennt auch das PDF)",
        required=True,
    )

    rechnungs_titel = forms.CharField(
        label="Rechnungstitel",
        max_length=100,
        initial="Programmierungsdienste {}".format(last_month_formated),
        help_text="(Ein Titel mit letztem Monat wurde automatisch "
                  "eingetragen)",
        required=True,
    )

    rechnungs_summe = forms.DecimalField(
        label="Summe ohne MwSt",
        help_text="(Zulässig sind Werte wie: 50, 50.5 oder 50,55)",
        localize=True,
        max_digits=10,
        decimal_places=2,
        required=True,
    )

    meine_daten = forms.ModelChoiceField(
        label="Arbeitnehmer",
        queryset=Arbeitnehmer.objects.all(),
        required=True,
    )

    def __init__(self, *args, **kwargs):
        """
        Fügt Feldern CSS und Bootstrap Styling hinzu.
        """
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.help_text_inline = True
        self.helper.layout = Layout(
            Field("firma", css_class="span4"),
            Field("rechnungs_nummer",
                  css_class="span4"),
            Field("rechnungs_titel",
                  css_class="span4"),
            Field(AppendedText("rechnungs_summe", "€",
                  css_class="span4",
                               )),
            Field("meine_daten", css_class="span4"),
        )
        super(RechnungsSummeForm, self).__init__(*args, **kwargs)


class UploadFileForm(forms.Form):
    """
    Das Formular für den File Upload auf CSV Import.
    """
    csv_file = forms.FileField(
        label="CSV Datei",
        required=True,
    )

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_tag = False
        super(UploadFileForm, self).__init__(*args, **kwargs)

    def clean_csv_file(self):
        """
        Ein Validator, der prüft, ob die Dateiendung ".csv" lautet.
        """
        data = self.cleaned_data["csv_file"]
        if str(data).split(".")[-1] != "csv":
            raise forms.ValidationError("Das ist keine CSV Datei!")
        return data
