# -*- coding: utf-8 -*-
import os
from datetime import date, time
from reportlab.lib.pagesizes import A4, cm
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer
from reportlab.platypus import Table, TableStyle
from reportlab.platypus.flowables import PageBreak
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.enums import TA_RIGHT, TA_LEFT, TA_CENTER
from reportlab.lib import colors
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont


def make_pdf(data, font_folder="stunden/ubuntu-font-family-0.80/",
             second_table=True):
    """
    Erstellt PDF Rechnungen.
    Nimmt ein dict als erstes Argument und optional ein Keyword Argument namens
    "font_folder", womit man angeben kann wo sich der Ordner der "Ubuntu"
    Schriftart befindet.

    Verwendungsbeispiel:
    make_pdf({
        "pdf_fileobject": u"filename.pdf",  # oder Django HttpResponse Objekt
        "pdf_title": u"Rechnung IT-0815",
        "pdf_author": u"Martin Fischer",
        "pdf_subject": u"Rechnung erstellt von webpystunden Software",
        "pdf_creator": u"webpystunden",
        "pdf_keywords": u"webpystunden, Martin, Fischer",
        "sender_address_name": u"John Cleese",
        "sender_address_street": u"Straße 15",
        "sender_address_city": u"Ort",
        "sender_address_zip_city": u"5555 Ort",
        "sender_address_country": u"Österreich",
        "sender_address_uid": u"UID: 123456789",
        "receiver_address_company": u"Firma",
        "receiver_address_name": u"Eric Idle",
        "receiver_address_street": u"Straße 99",
        "receiver_address_zip_city": u"9999 Ort",
        "receiver_address_country": u"Österreich",
        "receiver_address_uid": u"UID: 987654321",
        "rechnungs_nummer": u"Rechnung IT-0815",
        "rechnungs_titel": u"Rechnung für November",
        "rechnungs_summe_netto": u"100,00",
        "rechnungs_summe_mwst": u"20,00",
        "rechnungs_summe_brutto": u"120,00",
        "stunden_rows": [
            [date(2012, 11, 29),
            time(10, 00, 00,),
            time(12, 00, 00,),
            u"Das Protokoll Nr. 1",
            u"2.00"],
            [date(2012, 11, 30),
            time(13, 00, 00),
            time(16, 30, 00),
            u"Das Protokoll Nr. 2",
            u"3.50"]
        ],
        "stunden_gesamt_stunden": u"5.50",
        "sender_bank_receiver": u"John Cleese",
        "sender_bank_name": u"The Bank",
        "sender_bank_iban": u"AT00000000000000",
        "sender_bank_bic": u"XVSGHSVVVVVVVVV",
    }, font_folder="stunden/ubuntu-font-family-0.80/")
    """

    # Registriert die Schriftart Ubuntu.
    pdfmetrics.registerFont(TTFont("Ubuntu", os.path.join(font_folder,
                                                          "Ubuntu-R.ttf")))
    pdfmetrics.registerFont(TTFont("UbuntuBold", os.path.join(font_folder,
                            "Ubuntu-B.ttf")))
    pdfmetrics.registerFont(TTFont("UbuntuItalic", os.path.join(font_folder,
                            "Ubuntu-RI.ttf")))
    pdfmetrics.registerFontFamily("Ubuntu",
                                  normal="Ubuntu",
                                  bold="UbuntuBold",
                                  italic="UbuntuItalic")

    # Hier werden alles Styles aufgesetzt.
    page_width, page_height = A4
    font_size_p = 12
    font_size_h1 = 16
    color_h1 = colors.HexColor("#556AA6")

    styles = getSampleStyleSheet()

    styles.add(ParagraphStyle(name="p-left",
                              fontName="Ubuntu",
                              fontSize=font_size_p,
                              leading=font_size_p + 2,
                              leftIndent=0,
                              rightIndent=0,
                              firstLineIndent=0,
                              alignment=TA_LEFT,
                              spaceBefore=0,
                              spaceAfter=0,
                              bulletFontName="Ubuntu",
                              bulletFontSize=font_size_p,
                              bulletIndent=0,
                              textColor=colors.black,
                              backColor=None,
                              wordWrap=None,
                              borderWidth=0,
                              borderPadding=0,
                              borderColor=None,
                              borderRadius=None,
                              allowWidows=1,
                              allowOrphans=0))

    styles.add(ParagraphStyle(name="p-right",
                              fontName="Ubuntu",
                              fontSize=font_size_p,
                              leading=font_size_p + 2,
                              leftIndent=0,
                              rightIndent=0,
                              firstLineIndent=0,
                              alignment=TA_RIGHT,
                              spaceBefore=0,
                              spaceAfter=0,
                              bulletFontName="Ubuntu",
                              bulletFontSize=font_size_p,
                              bulletIndent=0,
                              textColor=colors.black,
                              backColor=None,
                              wordWrap=None,
                              borderWidth=0,
                              borderPadding=0,
                              borderColor=None,
                              borderRadius=None,
                              allowWidows=1,
                              allowOrphans=0))

    styles.add(ParagraphStyle(name="h1_left",
                              fontName="Ubuntu",
                              fontSize=font_size_h1,
                              leading=font_size_h1 + 2,
                              leftIndent=0,
                              rightIndent=0,
                              firstLineIndent=0,
                              alignment=TA_LEFT,
                              spaceBefore=0,
                              spaceAfter=0,
                              bulletFontName="Ubuntu",
                              bulletFontSize=font_size_h1,
                              bulletIndent=0,
                              textColor=color_h1,
                              backColor=None,
                              wordWrap=None,
                              borderWidth=0,
                              borderPadding=0,
                              borderColor=None,
                              borderRadius=None,
                              allowWidows=1,
                              allowOrphans=0))

    styles.add(ParagraphStyle(name="table-center",
                              fontName="Ubuntu",
                              fontSize=font_size_p,
                              leading=font_size_p + 2,
                              leftIndent=0,
                              rightIndent=0,
                              firstLineIndent=0,
                              alignment=TA_CENTER,
                              spaceBefore=0,
                              spaceAfter=0,
                              bulletFontName="Ubuntu",
                              bulletFontSize=font_size_p,
                              bulletIndent=0,
                              textColor=colors.black,
                              backColor=None,
                              wordWrap=None,
                              borderWidth=0,
                              borderPadding=0,
                              borderColor=None,
                              borderRadius=None,
                              allowWidows=1,
                              allowOrphans=0))

    styles.add(ParagraphStyle(name="table-left",
                              fontName="Ubuntu",
                              fontSize=font_size_p,
                              leading=font_size_p + 2,
                              leftIndent=0,
                              rightIndent=0,
                              firstLineIndent=0,
                              alignment=TA_LEFT,
                              spaceBefore=0,
                              spaceAfter=0,
                              bulletFontName="Ubuntu",
                              bulletFontSize=font_size_p,
                              bulletIndent=0,
                              textColor=colors.black,
                              backColor=None,
                              wordWrap=None,
                              borderWidth=0,
                              borderPadding=0,
                              borderColor=None,
                              borderRadius=None,
                              allowWidows=1,
                              allowOrphans=0))

    styles.add(ParagraphStyle(name="table-right",
                              fontName="Ubuntu",
                              fontSize=font_size_p,
                              leading=font_size_p + 2,
                              leftIndent=0,
                              rightIndent=0,
                              firstLineIndent=0,
                              alignment=TA_RIGHT,
                              spaceBefore=0,
                              spaceAfter=0,
                              bulletFontName="Ubuntu",
                              bulletFontSize=font_size_p,
                              bulletIndent=0,
                              textColor=colors.black,
                              backColor=None,
                              wordWrap=None,
                              borderWidth=0,
                              borderPadding=0,
                              borderColor=None,
                              borderRadius=None,
                              allowWidows=1,
                              allowOrphans=0))

    styles.add(ParagraphStyle(name="table-left-small",
                              fontName="Ubuntu",
                              fontSize=font_size_p - 2,
                              leading=font_size_p,
                              leftIndent=0,
                              rightIndent=0,
                              firstLineIndent=0,
                              alignment=TA_LEFT,
                              spaceBefore=0,
                              spaceAfter=0,
                              bulletFontName="Ubuntu",
                              bulletFontSize=font_size_p - 2,
                              bulletIndent=0,
                              textColor=colors.black,
                              backColor=None,
                              wordWrap=None,
                              borderWidth=0,
                              borderPadding=0,
                              borderColor=None,
                              borderRadius=None,
                              allowWidows=1,
                              allowOrphans=0))

    # So sieht die erste Seite aus.
    def first_page(canvas, doc):
        canvas.saveState()
        canvas.setFont("Ubuntu", 8)
        canvas.setFillColor(colors.gray)
        canvas.drawCentredString(page_width / 2.0, 20,
                                 "Seite {}".format(doc.page))
        canvas.restoreState()

    # So sehen alle weiter Seiten aus. Identisch zur ersten Seite.
    def further_pages(canvas, doc):
        canvas.saveState()
        canvas.setFont("Ubuntu", 8)
        canvas.setFillColor(colors.gray)
        canvas.drawCentredString(page_width / 2.0, 20,
                                 "Seite {}".format(doc.page))
        canvas.restoreState()

    # Die Story beginnt hier.
    story = []

    story.append(Spacer(1, font_size_p))

    # Die Daten des Rechnungssenders.
    sender_address_parts = [data["sender_address_name"],
                            data["sender_address_street"],
                            data["sender_address_zip_city"],
                            data["sender_address_country"],
                            data["sender_address_uid"]]

    for part in sender_address_parts:
        ptext = u"<font>{}</font>".format(part)
        story.append(Paragraph(ptext, styles["p-left"]))

    story.append(Spacer(1, font_size_p * 4))

    # Die Daten des Rechnungsempfängers.
    receiver_address_parts = [data["receiver_address_company"],
                              data["receiver_address_name"],
                              data["receiver_address_street"],
                              data["receiver_address_zip_city"],
                              data["receiver_address_country"],
                              data["receiver_address_uid"]]

    for part in receiver_address_parts:
        ptext = u"<font>{}</font>".format(part)
        story.append(Paragraph(ptext, styles["p-left"]))

    story.append(Spacer(1, font_size_p))

    # Der Ort und das Datum.
    sender_address_city = data["sender_address_city"]
    today = date.today()
    formated_date = today.strftime("%d.%m.%Y")
    ptext = u"<font>{}, {}</font>".format(sender_address_city, formated_date)
    story.append(Paragraph(ptext, styles["p-right"]))

    story.append(Spacer(1, font_size_p * 5))

    # Die Rechnungsnummer.
    ptext = u"""<font><b>Rechnung Nr: {}</b></font>
            """.format(data["rechnungs_nummer"])
    story.append(Paragraph(ptext, styles["h1_left"]))

    story.append(Spacer(1, font_size_p * 3))

    # Die Tabelle mit den Rechnungsdaten.
    # Überschriften.
    table_header_position = Paragraph(u"<b>Pos</b>", styles["table-center"])
    table_header_bezeichnung = Paragraph(u"<b>Bezeichnung</b>",
                                         styles["table-center"])
    table_header_summe = Paragraph(u"<b>Summe</b>", styles["table-center"])

    # Reihe 1 - Titel und Summe Netto.
    table_row1_position = Paragraph(u"1", styles["table-center"])
    table_row1_bezeichnung = Paragraph(u"{}".format(data["rechnungs_titel"]),
                                       styles["table-center"])
    table_row1_summe = Paragraph(u"€ {}".format(data["rechnungs_summe_netto"]),
                                 styles["table-right"])

    # Reihe 3 - Summe MWST.
    table_row3_position = Paragraph("", styles["table-center"])
    table_row3_bezeichnung = Paragraph(u"+ 20% MwSt", styles["table-right"])
    table_row3_summe = Paragraph(u"€ {}".format(data["rechnungs_summe_mwst"]),
                                 styles["table-right"])

    # Reihe 5 - Info zur Stundentabelle.
    table_row5_position = Paragraph("", styles["table-center"])
    table_row5_bezeichnung = Paragraph(u"""
                    <i>(Detailierte Stundenaufstellung auf Seite 2)</i>""",
                                       styles["table-left-small"])
    table_row5_summe = Paragraph("", styles["table-right"])

    # Reihe 7 - Summe Netto.
    table_row7_position = Paragraph("", styles["table-center"])
    table_row7_bezeichnung = Paragraph(u"Summe netto", styles["table-right"])
    table_row7_summe = Paragraph(u"€ {}".format(data["rechnungs_summe_netto"]),
                                 styles["table-right"])

    # Reihe 9 - Summe MWST.
    table_row9_position = Paragraph("", styles["table-center"])
    table_row9_bezeichnung = Paragraph(u"MwSt.", styles["table-right"])
    table_row9_summe = Paragraph(u"€ {}".format(data["rechnungs_summe_mwst"]),
                                 styles["table-right"])

    # Reihe 10 - Ein paar Striche zur Abgrenzung.
    table_row10_position = Paragraph("", styles["table-center"])
    table_row10_bezeichnung = Paragraph(u"-" * 23, styles["table-right"])
    table_row10_summe = Paragraph("", styles["table-right"])

    # Reihe 11 - Gesamtbetrag und Summe Brutto.
    table_row11_position = Paragraph("", styles["table-center"])
    table_row11_bezeichnung = Paragraph("<b>Gesamtbetrag</b>",
                                        styles["table-right"])
    table_row11_summe = Paragraph(u"<b>€ {}</b>".format(
                                  data["rechnungs_summe_brutto"]),
                                  styles["table-right"])

    # Die Tabelle als Liste aus Listen. Leer bedeutet eine leere Reihe.
    if second_table:
        data_rechnung = [
            [table_header_position, table_header_bezeichnung,
             table_header_summe],
            [table_row1_position, table_row1_bezeichnung, table_row1_summe],
            [],
            [table_row3_position, table_row3_bezeichnung, table_row3_summe],
            [],
            [table_row5_position, table_row5_bezeichnung, table_row5_summe],
            [],
            [table_row7_position, table_row7_bezeichnung, table_row7_summe],
            [],
            [table_row9_position, table_row9_bezeichnung, table_row9_summe],
            [table_row10_position, table_row10_bezeichnung, table_row10_summe],
            [table_row11_position, table_row11_bezeichnung, table_row11_summe],
        ]
    else:
        data_rechnung = [
            [table_header_position, table_header_bezeichnung,
             table_header_summe],
            [table_row1_position, table_row1_bezeichnung, table_row1_summe],
            [],
            [table_row3_position, table_row3_bezeichnung, table_row3_summe],
            [],
            [table_row7_position, table_row7_bezeichnung, table_row7_summe],
            [],
            [table_row9_position, table_row9_bezeichnung, table_row9_summe],
            [table_row10_position, table_row10_bezeichnung, table_row10_summe],
            [table_row11_position, table_row11_bezeichnung, table_row11_summe],
        ]

    # Styles für die Tabelle.
    table_rechnung = Table(data_rechnung,
                           colWidths=[1.5 * cm, 11 * cm, 3 * cm])

    table_rechnung.setStyle(TableStyle([
                            ("BACKGROUND", (0, 0), (-1, 0), colors.lightblue),
                            ("FONTNAME", (0, 0), (-1, -1), "Ubuntu"),
                            ("FONTSIZE", (0, 0), (-1, -1), 12),
                            ("INNERGRID", (0, 0), (-1, -1), 0.25,
                             colors.black),
                            ("BOX", (0, 0), (-1, -1), 0.25, colors.black),
                            ("VALIGN", (0, 0), (-1, -1), "MIDDLE")]))

    # Die Story wird um eine Tabelle reicher.
    story.append(table_rechnung)

    story.append(Spacer(1, font_size_p * 3))

    # Dei Bankinformationen.
    ptext = u"<font>Bitte den Gesamtbetrag überweisen an:</font>"
    story.append(Paragraph(ptext, styles["p-left"]))

    story.append(Spacer(1, font_size_p))

    sender_bank_parts = [u"Empfänger: {}".format(data["sender_bank_receiver"]),
                         u"Bank: {}".format(data["sender_bank_name"]),
                         u"IBAN: {}".format(data["sender_bank_iban"]),
                         u"BIC: {}".format(data["sender_bank_bic"])]

    for part in sender_bank_parts:
        ptext = u"<font>{}</font>".format(part)
        story.append(Paragraph(ptext, styles["p-left"]))

    if second_table:
        # Hier wird die erste Seite beendet.
        story.append(PageBreak())

        story.append(Spacer(1, font_size_p))

        # Die Überschrift auf Seite 2.
        ptext = u"<font><b>Stundenaufstellung</b></font>"
        story.append(Paragraph(ptext, styles["h1_left"]))

        story.append(Spacer(1, font_size_p * 3))

        # Die Tabelle mit den Stundenaufstellungen.
        stunden_table_data = []
        # Überschriften.
        table_datum_header = Paragraph(u"<b>Datum</b>", styles["table-center"])
        table_startzeit_header = Paragraph(u"<b>Startzeit</b>",
                                           styles["table-center"])
        table_endzeit_header = Paragraph(u"<b>Endzeit</b>",
                                         styles["table-center"])
        table_protokoll_header = Paragraph(u"<b>Protokoll</b>",
                                           styles["table-center"])
        table_stunden_header = Paragraph(u"<b>Stunden</b>",
                                         styles["table-center"])

        stunden_table_data_header = [table_datum_header,
                                     table_startzeit_header,
                                     table_endzeit_header,
                                     table_protokoll_header,
                                     table_stunden_header]

        stunden_table_data.append(stunden_table_data_header)

        # Die Reihen mit den Stundendaten.
        stunden_table_data_rows = []
        for stunden_row in data["stunden_rows"]:
            datum = Paragraph(u"{}".format(
                              stunden_row[0].strftime("%d.%m.%Y")),
                              styles["table-center"])
            startzeit = Paragraph(u"{}".format(
                                  stunden_row[1].strftime("%H:%M")),
                                  styles["table-center"])
            endzeit = Paragraph(u"{}".format(stunden_row[2].strftime("%H:%M")),
                                styles["table-center"])
            protokoll = Paragraph(u"{}".format(stunden_row[3]),
                                  styles["table-left"])
            stunden = Paragraph(u"{}".format(stunden_row[4]),
                                styles["table-center"])
            row = [datum, startzeit, endzeit, protokoll, stunden]
            stunden_table_data_rows.append(row)

        for row in stunden_table_data_rows:
            stunden_table_data.append(row)

        # Die letzte Reihe mit den Gesamtstunden.
        table_datum_row_last = Paragraph("", styles["table-center"])
        table_startzeit_row_last = Paragraph("", styles["table-center"])
        table_endzeit_row_last = Paragraph("", styles["table-center"])
        table_protokoll_row_last = Paragraph(u"<b>Gesamtstunden</b>",
                                             styles["table-right"])
        table_stunden_row_last = Paragraph(u"<b>{}</b>".format(
                                           data["stunden_gesamt_stunden"]),
                                           styles["table-center"])

        stunden_table_data_row_last = [table_datum_row_last,
                                       table_startzeit_row_last,
                                       table_endzeit_row_last,
                                       table_protokoll_row_last,
                                       table_stunden_row_last]

        stunden_table_data.append(stunden_table_data_row_last)

        # Styles für die Tabelle.
        table_stunden = Table(
            stunden_table_data,
            colWidths=[2.5 * cm, 2.2 * cm, 2.2 * cm, 7 * cm, 2 * cm]
        )

        table_stunden.setStyle(TableStyle([
            ("BACKGROUND", (0, 0), (-1, 0), colors.lightblue),
            ("FONTNAME", (0, 0), (-1, -1), "Ubuntu"),
            ("FONTSIZE", (0, 0), (-1, -1), 12),
            ("INNERGRID", (0, 0), (-1, -1), 0.25, colors.black),
            ("BOX", (0, 0), (-1, -1), 0.25, colors.black),
            ("VALIGN", (0, 0), (-1, -1), "MIDDLE"), ]))

        # Hier wird die Story um noch eine Tabelle reicher.
        story.append(table_stunden)

    # Das Template für das Dokument wird aufgesetzt.
    doc = SimpleDocTemplate(data["pdf_fileobject"],
                            pagesize=A4,
                            title=u"{}".format(data["pdf_title"]),
                            author=u"{}".format(data["pdf_author"]),
                            subject=u"{}".format(data["pdf_subject"]),
                            creator=u"{}".format(data["pdf_creator"]),
                            keywords=u"{}".format(data["pdf_keywords"]),
                            rightMargin=70,
                            leftMargin=70,
                            topMargin=20,
                            bottomMargin=20)

    # Das PDF wird erstellt.
    doc.build(story, onFirstPage=first_page, onLaterPages=further_pages)


if __name__ == '__main__':
    make_pdf({
        "pdf_fileobject": u"filename.pdf",  # oder Django HttpResponse Objekt
        "pdf_title": u"Rechnung IT-0815",
        "pdf_author": u"Martin Fischer",
        "pdf_subject": u"Rechnung erstellt von webpystunden Software",
        "pdf_creator": u"webpystunden",
        "pdf_keywords": u"webpystunden, Martin, Fischer",
        "sender_address_name": u"John Cleese",
        "sender_address_street": u"Straße 15",
        "sender_address_city": u"Ort",
        "sender_address_zip_city": u"5555 Ort",
        "sender_address_country": u"Österreich",
        "sender_address_uid": u"UID: 123456789",
        "receiver_address_company": u"Firma",
        "receiver_address_name": u"Eric Idle",
        "receiver_address_street": u"Straße 99",
        "receiver_address_zip_city": u"9999 Ort",
        "receiver_address_country": u"Österreich",
        "receiver_address_uid": u"UID: 987654321",
        "rechnungs_nummer": u"Rechnung IT-0815",
        "rechnungs_titel": u"Rechnung für November",
        "rechnungs_summe_netto": u"100,00",
        "rechnungs_summe_mwst": u"20,00",
        "rechnungs_summe_brutto": u"120,00",
        "stunden_rows": [
            [date(2012, 11, 29),
                time(10, 00, 00,),
                time(12, 00, 00,),
                u"Das Protokoll Nr. 1",
                u"2.00"],
            [date(2012, 11, 30),
                time(13, 00, 00),
                time(16, 30, 00),
                u"Das Protokoll Nr. 2",
                u"3.50"]
        ],
        "stunden_gesamt_stunden": u"5.50",
        "sender_bank_receiver": u"John Cleese",
        "sender_bank_name": u"The Bank",
        "sender_bank_iban": u"AT00000000000000",
        "sender_bank_bic": u"XVSGHSVVVVVVVVV",
    }, font_folder="stunden/ubuntu-font-family-0.80/", second_table=True)
