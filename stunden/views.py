# -*- coding: utf-8 -*-
import csv
from .models import StundenAufzeichnung, Firma, Arbeitnehmer
from .utils import calculate_stunden, moneyformat
from .pdf import make_pdf
from .forms import StundenAufzeichnungForm, RechnungsForm, UploadFileForm
from .forms import FirmaForm, ArbeitnehmerForm, RechnungsSummeForm
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.forms import ValidationError
from django.forms.util import ErrorList
from django.core.urlresolvers import reverse
from decimal import Decimal
from urllib import quote
from datetime import date


@login_required
def index(request):
    """
    Der View für die Index Seite.
    Angezeigt wird unteranderem eine Tabelle mit Pagination.
    Login ist notwendig.
    """
    stunden_list = StundenAufzeichnung.objects.all().select_related().order_by(
        "-datum",
        "-startzeit"
    )
    for row in stunden_list:
        row.stunden = calculate_stunden(
            row.startzeit,
            row.endzeit
        )
    paginator = Paginator(stunden_list, 10)
    page = request.GET.get("page")
    try:
        stundenaufzeichnung = paginator.page(page)
    except PageNotAnInteger:
        stundenaufzeichnung = paginator.page(1)
    except EmptyPage:
        stundenaufzeichnung = paginator.page(paginator.num_pages)

    return render_to_response("stunden/index.html",
                              {"stundenaufzeichnung": stundenaufzeichnung},
                              context_instance=RequestContext(request))


def log_in(request):
    """
    Der View für den Login.
    """
    error = ""
    username = ""
    if "next" in request.GET.keys():
        next = request.GET["next"]
    else:
        next = reverse("index")
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(next)
            else:
                error = "User ist inakiv."
        else:
            error = "Login fehlgeschlagen. Bitte nochmals versuchen."

    return render_to_response("stunden/login.html",
                              {"error": error, "username": username},
                              context_instance=RequestContext(request))


@login_required
def log_out(request):
    """
    Der View für den Logout.
    Leitet auf "/login/" um.
    Login ist notwendig.
    """
    logout(request)
    return HttpResponseRedirect(reverse("login"))


@login_required
def stundenaufzeichnung(request):
    """
    Der View, um eine Stundenaufzeichnung auszuwählen zum Bearbeiten oder zum
    Löschen.
    Login ist notwendig.
    """
    stunden_list = StundenAufzeichnung.objects.all().select_related().order_by(
        "-datum",
        "-startzeit"
    )
    paginator = Paginator(stunden_list, 10)
    page = request.GET.get("page")
    try:
        stundenaufzeichnung = paginator.page(page)
    except PageNotAnInteger:
        stundenaufzeichnung = paginator.page(1)
    except EmptyPage:
        stundenaufzeichnung = paginator.page(paginator.num_pages)

    return render_to_response("stunden/stundenaufzeichnung.html",
                              {"stundenaufzeichnung": stundenaufzeichnung},
                              context_instance=RequestContext(request))


@login_required
def stundenaufzeichnung_neu(request):
    """
    Der View für einen neuen Eintrag.
    Leitet auf "/" um.
    Login ist notwendig.
    """
    # Falls POST
    if request.method == "POST":
        form = StundenAufzeichnungForm(request.POST)
        if form.is_valid():
            stundenaufzeichnung = StundenAufzeichnungForm(request.POST)
            neuer_eintrag = stundenaufzeichnung.save()
            if neuer_eintrag:
                return HttpResponseRedirect(reverse("index"))

    # Wenn nicht POST dann ein leeres Formular.
    else:
        form = StundenAufzeichnungForm()

    return render_to_response("stunden/stundenaufzeichnung_neu.html",
                              {"form": form},
                              context_instance=RequestContext(request))


@login_required
def stundenaufzeichnung_bearbeiten(request, stundenaufzeichnung_id):
    """
    Der View, um eine Stundenaufzeichnung zu bearbeiten, oder zu löschen.
    Login ist notwendig.
    """
    # Request ist POST
    if request.method == "POST":
        # Bearbeiten
        if "stundenaufzeichnung_bearbeiten" in request.POST:
            stundenaufzeichnung = StundenAufzeichnungForm(
                request.POST,
                instance=StundenAufzeichnung.objects.get(
                    pk=stundenaufzeichnung_id))
            stundenaufzeichnung.save()
            return HttpResponseRedirect(reverse("index"))
        # Löschen
        elif "stundenaufzeichnung_loeschen" in request.POST:
            stundenaufzeichnung = StundenAufzeichnung.objects.get(
                pk=stundenaufzeichnung_id)
            stundenaufzeichnung.delete()
            return HttpResponseRedirect(reverse("index"))
        else:
            return HttpResponseRedirect(reverse("index"))

    # Request ist nicht POST
    else:
        stundenaufzeichnung = get_object_or_404(StundenAufzeichnung,
                                                pk=stundenaufzeichnung_id)
        form = StundenAufzeichnungForm(instance=stundenaufzeichnung)
        return render_to_response(
            "stunden/stundenaufzeichnung_bearbeiten.html",
            {"form": form,
             "stundenaufzeichnung_id": stundenaufzeichnung_id},
            context_instance=RequestContext(request))


@login_required
def firma(request):
    """
    Der View, um eine Firma auszuwählen zum Bearbeiten oder zum Löschen.
    Login ist notwendig.
    """
    firmen_list = Firma.objects.all().order_by("firma")
    paginator = Paginator(firmen_list, 10)
    page = request.GET.get("page")
    try:
        firma = paginator.page(page)
    except PageNotAnInteger:
        firma = paginator.page(1)
    except EmptyPage:
        firma = paginator.page(paginator.num_pages)

    return render_to_response("stunden/firma.html",
                              {"firma": firma},
                              context_instance=RequestContext(request))


@login_required
def firma_neu(request):
    """
    Der View für einen neuen Firma Eintrag.
    Leitet auf "/" um.
    Login ist notwendig.
    """
    # Falls POST
    if request.method == "POST":
        form = FirmaForm(request.POST)
        if form.is_valid():
            firma = FirmaForm(request.POST)
            neuer_eintrag = firma.save()
            if neuer_eintrag:
                return HttpResponseRedirect(reverse("stundenaufzeichnung_neu"))

    # Wenn nicht POST dann ein leeres Formular.
    else:
        form = FirmaForm()

    return render_to_response("stunden/firma_neu.html",
                              {"form": form},
                              context_instance=RequestContext(request))


@login_required
def firma_bearbeiten(request, firma_id):
    """
    Der View, um eine Firma zu bearbeiten, oder zu löschen.
    Login ist notwendig.
    """
    # Request ist POST
    if request.method == "POST":
        # Bearbeiten
        if "firma_bearbeiten" in request.POST:
            firma = FirmaForm(
                request.POST,
                instance=Firma.objects.get(pk=firma_id))
            firma.save()
            return HttpResponseRedirect(reverse("firma"))
        # Löschen
        elif "firma_loeschen" in request.POST:
            firma = Firma.objects.get(pk=firma_id)
            firma.delete()
            return HttpResponseRedirect(reverse("firma"))
        else:
            return HttpResponseRedirect(reverse("firma"))

    # Request ist nicht POST
    else:
        firma = get_object_or_404(Firma, pk=firma_id)
        form = FirmaForm(instance=firma)
        return render_to_response(
            "stunden/firma_bearbeiten.html",
            {"form": form,
             "firma_id": firma_id},
            context_instance=RequestContext(request))


@login_required
def arbeitnehmer(request):
    """
    Der View, um eine Arbeitnehmer auszuwählen zum Bearbeiten oder zum Löschen.
    Login ist notwendig.
    """
    arbeitnehmer_list = Arbeitnehmer.objects.all().order_by("name")
    paginator = Paginator(arbeitnehmer_list, 10)
    page = request.GET.get("page")
    try:
        arbeitnehmer = paginator.page(page)
    except PageNotAnInteger:
        arbeitnehmer = paginator.page(1)
    except EmptyPage:
        arbeitnehmer = paginator.page(paginator.num_pages)

    return render_to_response("stunden/arbeitnehmer.html",
                              {"arbeitnehmer": arbeitnehmer},
                              context_instance=RequestContext(request))


@login_required
def arbeitnehmer_neu(request):
    """
    Der View für einen neuen Firma Eintrag.
    Leitet auf "/" um.
    Login ist notwendig.
    """
    # Falls POST
    if request.method == "POST":
        form = ArbeitnehmerForm(request.POST)
        if form.is_valid():
            arbeitnehmer = ArbeitnehmerForm(request.POST)
            neuer_eintrag = arbeitnehmer.save()
            if neuer_eintrag:
                return HttpResponseRedirect(reverse("stundenaufzeichnung_neu"))

    # Wenn nicht POST dann ein leeres Formular.
    else:
        form = ArbeitnehmerForm()

    return render_to_response("stunden/arbeitnehmer_neu.html",
                              {"form": form},
                              context_instance=RequestContext(request))


@login_required
def arbeitnehmer_bearbeiten(request, arbeitnehmer_id):
    """
    Der View, um einen Arbeitnehmer zu bearbeiten, oder zu löschen.
    Login ist notwendig.
    """
    # Request ist POST
    if request.method == "POST":
        # Bearbeiten
        if "arbeitnehmer_bearbeiten" in request.POST:
            arbeitnehmer = ArbeitnehmerForm(
                request.POST,
                instance=Arbeitnehmer.objects.get(pk=arbeitnehmer_id))
            arbeitnehmer.save()
            return HttpResponseRedirect(reverse("arbeitnehmer"))
        # Löschen
        elif "arbeitnehmer_loeschen" in request.POST:
            arbeitnehmer = Arbeitnehmer.objects.get(pk=arbeitnehmer_id)
            arbeitnehmer.delete()
            return HttpResponseRedirect(reverse("arbeitnehmer"))
        else:
            return HttpResponseRedirect(reverse("arbeitnehmer"))

    # Request ist nicht POST
    else:
        arbeitnehmer = get_object_or_404(Arbeitnehmer, pk=arbeitnehmer_id)
        form = ArbeitnehmerForm(instance=arbeitnehmer)
        return render_to_response(
            "stunden/arbeitnehmer_bearbeiten.html",
            {"form": form,
             "arbeitnehmer_id": arbeitnehmer_id},
            context_instance=RequestContext(request))


@login_required
def rechnung(request):
    """
    Der View für die Rechnung.
    Bei einem GET Request wird ein Formular angezeigt und eine Tabelle mit
    Einträgen, die unbezahlt markiert sind.
    Bei einem POST Request wird das Formular überprüft und wenn alles richtig
    scheint wird eine PDF Rechnung mit Hilfe von pdf.make_pdf() erstellt.
    Login ist notwendig.
    """
    # Holt alle unbezahlten Einträge aus der db.
    stunden_not_payed = StundenAufzeichnung.objects.select_related() \
        .filter(bezahlt=False).order_by("-datum", "-startzeit")

    # Rechnet die Stunden aus.
    for row in stunden_not_payed:
        row.stunden = calculate_stunden(row.startzeit, row.endzeit)

    # Eine Liste der markierten Checkboxen bei einem POST Request.
    stunden_ids = []

    if request.method == "POST":
        form = RechnungsForm(request.POST)
        stunden_ids = request.POST.getlist("checks[]")
        stunden_ids = [int(id) for id in stunden_ids]
        custom_error = "Bitte mindestens einen Eintrag unten auswählen"

        # Einträge als bezahlt markieren, update in der db.
        if "bezahlt_markieren" in request.POST and stunden_ids:
            queryset = StundenAufzeichnung.objects.filter(pk__in=stunden_ids)
            queryset.update(bezahlt=True)
            return HttpResponseRedirect(reverse("index"))

        # Formular nicht valid, keine Einträge gewählt, nicht bezahlt_markieren
        if not form.is_valid() and not stunden_ids and not \
                "bezahlt_markieren" in request.POST:
            return render_to_response("stunden/rechnung.html",
                                      {
                                      "stunden_not_payed": stunden_not_payed,
                                      "form": form,
                                      "custom_error": custom_error
                                      },
                                      context_instance=RequestContext(request))

        # # Formular nicht valid, keine Einträge gewählt, bezahlt_markieren.
        elif not form.is_valid() and not stunden_ids and \
                "bezahlt_markieren" in request.POST:
            form = RechnungsForm()
            return render_to_response("stunden/rechnung.html",
                                      {
                                      "stunden_not_payed": stunden_not_payed,
                                      "form": form,
                                      "custom_error": custom_error
                                      },
                                      context_instance=RequestContext(request))

        # Formular ist valid.
        if form.is_valid():
            # Keine Einträge gewählt.
            if not stunden_ids:
                return render_to_response(
                    "stunden/rechnung.html",
                    {"stunden_not_payed": stunden_not_payed,
                    "form": form,
                    "custom_error": custom_error},
                    context_instance=RequestContext(request))

            # Die Daten aus dem POST Request
            receiver_address_company = form.cleaned_data["firma"].firma
            receiver_address_name = form.cleaned_data["firma"].name
            receiver_address_street = form.cleaned_data["firma"].adresse
            receiver_address_zip = form.cleaned_data["firma"].plz
            receiver_address_city = form.cleaned_data["firma"].ort
            receiver_address_zip_city = receiver_address_zip + " " + \
                receiver_address_city
            receiver_address_country = form.cleaned_data["firma"].land
            receiver_address_uid = form.cleaned_data["firma"].uid
            rechnungs_nummer = form.cleaned_data["rechnungs_nummer"]
            rechnungs_titel = form.cleaned_data["rechnungs_titel"]
            rechnungs_stundenlohn = form.cleaned_data["rechnungs_stundenlohn"]
            sender_address_name = form.cleaned_data["meine_daten"].name
            sender_address_street = form.cleaned_data["meine_daten"].adresse
            sender_address_zip = form.cleaned_data["meine_daten"].plz
            sender_address_city = form.cleaned_data["meine_daten"].ort
            sender_address_zip_city = sender_address_zip + " " + \
                sender_address_city
            sender_address_country = form.cleaned_data["meine_daten"].land
            sender_address_uid = form.cleaned_data["meine_daten"].uid
            sender_bank_receiver = sender_address_name
            sender_bank_name = form.cleaned_data["meine_daten"].bank_name
            sender_bank_iban = form.cleaned_data["meine_daten"].bank_iban
            sender_bank_bic = form.cleaned_data["meine_daten"].bank_bic

            # Die Stundenreihen werden erstellt.
            stunden_rows = []
            stunden_gesamt_stunden = 0
            for id in stunden_ids:
                entry = StundenAufzeichnung.objects.get(pk=id)
                if entry.firma.firma == receiver_address_company:
                    stunden = Decimal(calculate_stunden(entry.startzeit,
                                                        entry.endzeit))
                    stunden_gesamt_stunden += stunden
                    row = [
                        entry.datum,
                        entry.startzeit,
                        entry.endzeit,
                        entry.protokoll,
                        stunden,
                    ]
                    stunden_rows.append(row)

            # Fehler, wenn kein gewählter Eintrag zum Rechnungsempfänger passt.
            if not stunden_rows:
                custom_error = """Es wurde kein Eintrag für den oben
                ausgewählten Rechnungsempfänger ausgewählt."""
                return render_to_response(
                    "stunden/rechnung.html",
                    {"stunden_not_payed": stunden_not_payed,
                     "form": form,
                     "stunden_ids": stunden_ids,
                     "custom_error": custom_error},
                    context_instance=RequestContext(request)
                )

            # Die Rechnungssummen werden ausgerechnet.
            rechnungs_summe_netto = stunden_gesamt_stunden * \
                rechnungs_stundenlohn
            rechnungs_summe_mwst = rechnungs_summe_netto / Decimal(100) \
                * Decimal(20)
            rechnungs_summe_brutto = rechnungs_summe_netto +\
                rechnungs_summe_mwst

            rechnungs_summe_netto_formated = moneyformat(rechnungs_summe_netto)
            rechnungs_summe_mwst_formated = moneyformat(rechnungs_summe_mwst)
            rechnungs_summe_brutto_formated = moneyformat(
                rechnungs_summe_brutto)

            #Das file Objekt zur PDF erstellung wird erstellt.
            response = HttpResponse(mimetype="application/pdf")
            filename = rechnungs_nummer.encode("utf-8")
            filename = quote(filename)
            url = u"attachment; filename=\"Rechnung {}.pdf\"".format(filename)
            response["Content-Disposition"] = url

            # Die Daten fürs PDF.
            data = {
                "pdf_fileobject": response,
                "pdf_title": u"Rechnung {}".format(rechnungs_nummer),
                "pdf_author": u"Martin Fischer",
                "pdf_subject": u"Rechnung erstellt von webpystunden Software",
                "pdf_creator": u"webpystunden",
                "pdf_keywords": u"webpystunden, Martin, Fischer",
                "sender_address_name": sender_address_name,
                "sender_address_street": sender_address_street,
                "sender_address_city": sender_address_city,
                "sender_address_zip_city": sender_address_zip_city,
                "sender_address_country": sender_address_country,
                "sender_address_uid": sender_address_uid,
                "receiver_address_company": receiver_address_company,
                "receiver_address_name": receiver_address_name,
                "receiver_address_street": receiver_address_street,
                "receiver_address_zip_city": receiver_address_zip_city,
                "receiver_address_country": receiver_address_country,
                "receiver_address_uid": receiver_address_uid,
                "rechnungs_nummer": rechnungs_nummer,
                "rechnungs_titel": rechnungs_titel,
                "rechnungs_summe_netto": rechnungs_summe_netto_formated,
                "rechnungs_summe_mwst": rechnungs_summe_mwst_formated,
                "rechnungs_summe_brutto": rechnungs_summe_brutto_formated,
                "stunden_rows": stunden_rows,
                "stunden_gesamt_stunden": stunden_gesamt_stunden,
                "sender_bank_receiver": sender_bank_receiver,
                "sender_bank_name": sender_bank_name,
                "sender_bank_iban": sender_bank_iban,
                "sender_bank_bic": sender_bank_bic,
            }

            # Das PDF wird erstellt.
            make_pdf(data)

            # Das PDF wird an den Browser zum Herunterladen geschickt.
            return response

    # Falls kein POST Request.
    else:
        form = RechnungsForm()

    return render_to_response("stunden/rechnung.html",
                              {
                              "stunden_not_payed": stunden_not_payed,
                              "form": form,
                              "stunden_ids": stunden_ids,
                              },
                              context_instance=RequestContext(request))


@login_required
def rechnung_summe(request):
    """
    Der View für die Rechnung mit einer Gesamtsumme.
    Bei einem GET Request wird ein Formular angezeigt und eine Tabelle mit
    Einträgen, die unbezahlt markiert sind.
    Bei einem POST Request wird das Formular überprüft und wenn alles richtig
    scheint wird eine PDF Rechnung mit Hilfe von pdf.make_pdf() erstellt.
    Login ist notwendig.
    """
    if request.method == "POST":
        form = RechnungsSummeForm(request.POST)

        # Formular nicht valid
        if not form.is_valid():
            return render_to_response("stunden/rechnungsumme.html",
                                      {
                                      "form": form,
                                      },
                                      context_instance=RequestContext(request))

        # Formular ist valid.
        if form.is_valid():
            # Die Daten aus dem POST Request
            receiver_address_company = form.cleaned_data["firma"].firma
            receiver_address_name = form.cleaned_data["firma"].name
            receiver_address_street = form.cleaned_data["firma"].adresse
            receiver_address_zip = form.cleaned_data["firma"].plz
            receiver_address_city = form.cleaned_data["firma"].ort
            receiver_address_zip_city = receiver_address_zip + " " + \
                receiver_address_city
            receiver_address_country = form.cleaned_data["firma"].land
            receiver_address_uid = form.cleaned_data["firma"].uid
            rechnungs_nummer = form.cleaned_data["rechnungs_nummer"]
            rechnungs_titel = form.cleaned_data["rechnungs_titel"]
            rechnungs_summe = form.cleaned_data["rechnungs_summe"]
            sender_address_name = form.cleaned_data["meine_daten"].name
            sender_address_street = form.cleaned_data["meine_daten"].adresse
            sender_address_zip = form.cleaned_data["meine_daten"].plz
            sender_address_city = form.cleaned_data["meine_daten"].ort
            sender_address_zip_city = sender_address_zip + " " + \
                sender_address_city
            sender_address_country = form.cleaned_data["meine_daten"].land
            sender_address_uid = form.cleaned_data["meine_daten"].uid
            sender_bank_receiver = sender_address_name
            sender_bank_name = form.cleaned_data["meine_daten"].bank_name
            sender_bank_iban = form.cleaned_data["meine_daten"].bank_iban
            sender_bank_bic = form.cleaned_data["meine_daten"].bank_bic

            # Die Rechnungssummen werden ausgerechnet.
            rechnungs_summe_netto = rechnungs_summe
            rechnungs_summe_mwst = rechnungs_summe_netto / Decimal(100) \
                * Decimal(20)
            rechnungs_summe_brutto = rechnungs_summe_netto +\
                rechnungs_summe_mwst

            rechnungs_summe_netto_formated = moneyformat(rechnungs_summe_netto)
            rechnungs_summe_mwst_formated = moneyformat(rechnungs_summe_mwst)
            rechnungs_summe_brutto_formated = moneyformat(
                rechnungs_summe_brutto)

            #Das file Objekt zur PDF erstellung wird erstellt.
            response = HttpResponse(mimetype="application/pdf")
            filename = rechnungs_nummer.encode("utf-8")
            filename = quote(filename)
            url = u"attachment; filename=\"Rechnung {}.pdf\"".format(filename)
            response["Content-Disposition"] = url

            # Die Daten fürs PDF.
            data = {
                "pdf_fileobject": response,
                "pdf_title": u"Rechnung {}".format(rechnungs_nummer),
                "pdf_author": u"Martin Fischer",
                "pdf_subject": u"Rechnung erstellt von webpystunden Software",
                "pdf_creator": u"webpystunden",
                "pdf_keywords": u"webpystunden, Martin, Fischer",
                "sender_address_name": sender_address_name,
                "sender_address_street": sender_address_street,
                "sender_address_city": sender_address_city,
                "sender_address_zip_city": sender_address_zip_city,
                "sender_address_country": sender_address_country,
                "sender_address_uid": sender_address_uid,
                "receiver_address_company": receiver_address_company,
                "receiver_address_name": receiver_address_name,
                "receiver_address_street": receiver_address_street,
                "receiver_address_zip_city": receiver_address_zip_city,
                "receiver_address_country": receiver_address_country,
                "receiver_address_uid": receiver_address_uid,
                "rechnungs_nummer": rechnungs_nummer,
                "rechnungs_titel": rechnungs_titel,
                "rechnungs_summe_netto": rechnungs_summe_netto_formated,
                "rechnungs_summe_mwst": rechnungs_summe_mwst_formated,
                "rechnungs_summe_brutto": rechnungs_summe_brutto_formated,
                "sender_bank_receiver": sender_bank_receiver,
                "sender_bank_name": sender_bank_name,
                "sender_bank_iban": sender_bank_iban,
                "sender_bank_bic": sender_bank_bic,
            }

            # Das PDF wird erstellt.
            make_pdf(data, second_table=False)

            # Das PDF wird an den Browser zum Herunterladen geschickt.
            return response

    # Falls kein POST Request.
    else:
        form = RechnungsSummeForm()

    return render_to_response("stunden/rechnungsumme.html",
                              {
                              "form": form,
                              },
                              context_instance=RequestContext(request))


@login_required
def csvexport(request):
    """
    Der View für den CSV Export.
    Login ist notwendig.
    """
    # Hole alle Daten.
    stundenaufzeichnungen = StundenAufzeichnung.objects.all().select_related()

    # Wenn POST Request.
    if request.method == "POST":
        # File Objekt wird erstellt.
        response = HttpResponse(mimetype="text/csv")
        today = date.today()
        filename = "webpystunden-export-{}".format(today)
        response["Content-Disposition"] = "attachment; filename={}.csv".format(
                                          filename)
        # Der CSV Writer.
        writer = csv.writer(response, delimiter=";")
        # Die Überschriften für die erste Zeile.
        field_names = [
            "Datum",
            "Firma Firmenname",
            "Firma Name",
            "Firma Adresse",
            "Firma PLZ",
            "Firma Ort",
            "Firma Land",
            "Firma UID",
            "Startzeit",
            "Endzeit",
            "Arbeitnehmer Name",
            "Arbeitnehmer Adresse",
            "Arbeitnehmer PLZ",
            "Arbeitnehmer Ort",
            "Arbeitnehmer Land",
            "Arbeitnehmer UID",
            "Arbeitnehmer Bank Name",
            "Arbeitnehmer Bank IBAN",
            "Arbeitnehmer Bank BIC",
            "Protokoll",
            "Bezahlt",
        ]
        # Schreib die erste Zeile.
        writer.writerow(field_names)
        # Die Daten
        for item in stundenaufzeichnungen:
            row = [
                item.datum,
                item.firma.firma.encode("utf-8"),
                item.firma.name.encode("utf-8"),
                item.firma.adresse.encode("utf-8"),
                item.firma.plz.encode("utf-8"),
                item.firma.ort.encode("utf-8"),
                item.firma.land.encode("utf-8"),
                item.firma.uid.encode("utf-8"),
                item.startzeit,
                item.endzeit,
                item.arbeitnehmer.name.encode("utf-8"),
                item.arbeitnehmer.adresse.encode("utf-8"),
                item.arbeitnehmer.plz.encode("utf-8"),
                item.arbeitnehmer.ort.encode("utf-8"),
                item.arbeitnehmer.land.encode("utf-8"),
                item.arbeitnehmer.uid.encode("utf-8"),
                item.arbeitnehmer.bank_name.encode("utf-8"),
                item.arbeitnehmer.bank_iban.encode("utf-8"),
                item.arbeitnehmer.bank_bic.encode("utf-8"),
                item.protokoll.encode("utf-8"),
                item.bezahlt
            ]
            # Schreib die Zeile
            writer.writerow(row)

        # Schickt die CSV datei zum Browser zum Herunterladen.
        return response

    # Falls nicht POST Request.
    else:
        return render_to_response(
            "stunden/csvexport.html",
            {"stundenaufzeichnungen": stundenaufzeichnungen},
            context_instance=RequestContext(request)
        )


@login_required
def csvimport(request):
    """
    Der View für den CSV Import.
    Login ist notwendig.
    """
    # Wenn der Request POST ist.
    if request.method == "POST":
        # Hol die Datei.
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            # Das Datei.
            csv_file = request.FILES["csv_file"]

            data = []
            try:
                # CSV Reader liest die Datei in eine Liste.
                data = [row for row in csv.reader(csv_file, delimiter=";")]
                # Die Überschriften in der ersten Zeile.
                header = data.pop(0)
                field_names = [
                    "Datum",
                    "Firma Firmenname",
                    "Firma Name",
                    "Firma Adresse",
                    "Firma PLZ",
                    "Firma Ort",
                    "Firma Land",
                    "Firma UID",
                    "Startzeit",
                    "Endzeit",
                    "Arbeitnehmer Name",
                    "Arbeitnehmer Adresse",
                    "Arbeitnehmer PLZ",
                    "Arbeitnehmer Ort",
                    "Arbeitnehmer Land",
                    "Arbeitnehmer UID",
                    "Arbeitnehmer Bank Name",
                    "Arbeitnehmer Bank IBAN",
                    "Arbeitnehmer Bank BIC",
                    "Protokoll",
                    "Bezahlt",
                ]
                # Überprüft, ob die Überschriften in der ersten Zeile stimmen.
                if header != field_names:
                    errors = form._errors.setdefault("csv_file", ErrorList())
                    errors.append(u"""Diese CSV Datei scheint kein webpystunden
                                  Backup (Export) zu sein und kann daher nicht
                                  importiert werden!""")
                    raise ValidationError()
            except:
                return render_to_response(
                    "stunden/csvimport.html",
                    {"form": form},
                    context_instance=RequestContext(request)
                )

            # Teilt die Daten in drei Listen, je nach Zugehörigkeit.
            data_firma = []
            data_arbeitnehmer = []
            data_stundenaufzeichnung = []
            for row in data:
                row_firma = [row[1], row[2], row[3], row[4], row[5], row[6],
                             row[7]]
                data_firma.append(row_firma)

                row_arbeitnehmer = [row[10], row[11], row[12], row[13],
                                    row[14], row[15], row[16], row[17],
                                    row[18]]
                data_arbeitnehmer.append(row_arbeitnehmer)

                row_stundenaufzeichnung = [row[0], row_firma[0], row[8],
                                           row[9], row_arbeitnehmer[0],
                                           row[19], row[20]]
                data_stundenaufzeichnung.append(row_stundenaufzeichnung)

            data_firma = dict((x[0], x) for x in data_firma).values()
            data_arbeitnehmer = dict((x[0], x) for x in data_arbeitnehmer) \
                .values()

            # Firma >> db und merkt sich die Anzahl.
            count_firma = 0
            for row in data_firma:
                obj, created = Firma.objects.get_or_create(firma=row[0],
                                                           name=row[1],
                                                           adresse=row[2],
                                                           plz=row[3],
                                                           ort=row[4],
                                                           land=row[5],
                                                           uid=row[6])
                if created:
                    count_firma += 1

            # Arbeitnehmer >> db und merkt sich die Anzahl.
            count_arbeitnehmer = 0
            for row in data_arbeitnehmer:
                obj, created = Arbeitnehmer.objects.get_or_create(
                    name=row[0],
                    adresse=row[1],
                    plz=row[2],
                    ort=row[3],
                    land=row[4],
                    uid=row[5],
                    bank_name=row[6],
                    bank_iban=row[7],
                    bank_bic=row[8]
                )
                if created:
                    count_arbeitnehmer += 1

            # StundenAufzeichnung >> db und merkt sich die Anzahl.
            count_stundenaufzeichnung = 0
            for row in data_stundenaufzeichnung:
                firma_firma = row[1]
                firma = Firma.objects.get(firma=firma_firma)
                arbeitnehmer_name = row[4]
                arbeitnehmer = Arbeitnehmer.objects.get(name=arbeitnehmer_name)
                if row[6] == "True":
                    bezahlt = True
                elif row[6] == "False":
                    bezahlt = False
                obj, created = StundenAufzeichnung.objects.get_or_create(
                    datum=row[0],
                    firma=firma,
                    startzeit=row[2],
                    endzeit=row[3],
                    arbeitnehmer=arbeitnehmer,
                    protokoll=row[5],
                    bezahlt=bezahlt
                )
                if created:
                    count_stundenaufzeichnung += 1

            # Leitet auf eine success Seite um und schickt die Anzahlen mit.
            return HttpResponseRedirect(reverse(
                "csvimport_success", kwargs={
                "count_stundenaufzeichnung": count_stundenaufzeichnung,
                "count_firma": count_firma,
                "count_arbeitnehmer": count_arbeitnehmer})
            )

    # Falls der Request nicht POST ist.
    else:
        form = UploadFileForm()

    return render_to_response("stunden/csvimport.html",
                              {"form": form},
                              context_instance=RequestContext(request))


@login_required
def csvimport_success(request, count_stundenaufzeichnung, count_firma,
                      count_arbeitnehmer):
    """
    Der View für die CSV Import Success Seite.
    Holt sich die Anzahlen der db Imports aus der URL.
    Login ist notwendig.
    """

    return render_to_response(
        "stunden/csvimport_success.html",
        {"count_stundenaufzeichnung": count_stundenaufzeichnung,
        "count_firma": count_firma,
        "count_arbeitnehmer": count_arbeitnehmer},
        context_instance=RequestContext(request)
    )


@login_required
def api_help(request):
    """
    Der View für die API Hilfe Seite.
    Login ist notwendig.
    """

    return render_to_response("stunden/api_help.html",
                              {},
                              context_instance=RequestContext(request))
