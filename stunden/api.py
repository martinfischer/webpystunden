# -*- coding: utf-8 -*-
from .models import StundenAufzeichnung, Firma, Arbeitnehmer
from tastypie.resources import ModelResource
from tastypie import fields
from tastypie.authentication import ApiKeyAuthentication
from tastypie.authorization import DjangoAuthorization
from tastypie.throttle import BaseThrottle
from tastypie.serializers import Serializer


class FirmaResource(ModelResource):
    """
    Das API Model für Firma.
    """
    class Meta:
        queryset = Firma.objects.all()
        resource_name = "firma"
        allowed_methods = ["get"]
        authentication = ApiKeyAuthentication()
        authorization = DjangoAuthorization()
        serializer = Serializer(formats=["json", "jsonp"])
        throttle = BaseThrottle()


class ArbeitnehmerResource(ModelResource):
    """
    Das API Model für Arbeitnehmer.
    """
    class Meta:
        queryset = Arbeitnehmer.objects.all()
        resource_name = "arbeitnehmer"
        allowed_methods = ["get"]
        authentication = ApiKeyAuthentication()
        authorization = DjangoAuthorization()
        serializer = Serializer(formats=["json", "jsonp"])
        throttle = BaseThrottle()


class StundenResource(ModelResource):
    """
    Das API Model für StundenAufzeichnung.
    """
    firma = fields.ForeignKey(FirmaResource, "firma", full=True)
    arbeitnehmer = fields.ForeignKey(ArbeitnehmerResource, "arbeitnehmer",
                                     full=True)

    class Meta:
        queryset = StundenAufzeichnung.objects.all()
        resource_name = "stunden"
        allowed_methods = ["get"]
        authentication = ApiKeyAuthentication()
        authorization = DjangoAuthorization()
        serializer = Serializer(formats=["json", "jsonp"])
        throttle = BaseThrottle()
