# -*- coding: utf-8 -*-
from .api import StundenResource, FirmaResource, ArbeitnehmerResource
from django.conf.urls import patterns, url, include
from django.contrib import admin
from tastypie.api import Api


# Die Admin Seite.
admin.autodiscover()

# Setzt die API und die API Version auf und registriert die Modele.
v1_api = Api(api_name="v1")
v1_api.register(StundenResource())
v1_api.register(FirmaResource())
v1_api.register(ArbeitnehmerResource())


urlpatterns = patterns(
    'stunden.views',
    # Home
    url(r'^$', "index", name="index"),

    # Login und Logout
    url(r'^login/$', "log_in", name="login"),
    url(r'^logout/$', "log_out", name="logout"),

    # Rechnungen
    url(r'^rechnung/$', "rechnung", name="rechnung"),
    url(r'^rechnungsumme/$', "rechnung_summe", name="rechnungsumme"),

    # CSV Export
    url(r'^csvexport/$', "csvexport", name="csvexport"),

    # CSV Import und Success Seite
    url(r'^csvimport/$', "csvimport", name="csvimport"),
    url(r'^csvimport/success/(?P<count_stundenaufzeichnung>\d+)/'
        '(?P<count_firma>\d+)/(?P<count_arbeitnehmer>\d+)/$',
        "csvimport_success", name="csvimport_success"),

    # Stundenaufzeichnung, neu und bearbeiten
    url(r'^stundenaufzeichnung/$', "stundenaufzeichnung",
        name="stundenaufzeichnung"),
    url(r'^stundenaufzeichnung/neu/$', "stundenaufzeichnung_neu",
        name="stundenaufzeichnung_neu"),
    url(r'^stundenaufzeichnung/(?P<stundenaufzeichnung_id>\d+)/$',
        "stundenaufzeichnung_bearbeiten",
        name="stundenaufzeichnung_bearbeiten"),

    # Firma, neu und bearbeiten
    url(r'^firma/$', "firma", name="firma"),
    url(r'^firma/neu/$', "firma_neu", name="firma_neu"),
    url(r'^firma/(?P<firma_id>\d+)/$', "firma_bearbeiten",
        name="firma_bearbeiten"),

    # Arbeitnehmer, neu und bearbeiten
    url(r'^arbeitnehmer/$', "arbeitnehmer", name="arbeitnehmer"),
    url(r'^arbeitnehmer/neu/$', "arbeitnehmer_neu",
        name="arbeitnehmer_neu"),
    url(r'^arbeitnehmer/(?P<arbeitnehmer_id>\d+)/$', "arbeitnehmer_bearbeiten",
        name="arbeitnehmer_bearbeiten"),

    # Tastypie API
    url(r'^api/', include(v1_api.urls)),
    url(r'^api/$', "api_help", name="api_help"),

    # Admin
    url(r'^admin/', include(admin.site.urls)),
)
