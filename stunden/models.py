# -*- coding: utf-8 -*-
from .utils import calculate_stunden
from django.db import models
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from tastypie.models import create_api_key


# Sendet ein Signal, das bei Erstellung neuer User einen ApiKey kreiert.
# Kommentiert, da sonst beim ersten manage.py syncdb Fehler passieren.
# Kann nach der ersten migration entkommentiert werden.
models.signals.post_save.connect(create_api_key, sender=User)


class Firma(models.Model):
    """
    Das ORM Model für Firma.
    """
    firma = models.CharField(max_length=200, blank=False, unique=True)
    name = models.CharField(max_length=200, blank=True)
    adresse = models.CharField(max_length=200, blank=True)
    plz = models.CharField(max_length=20, blank=True)
    ort = models.CharField(max_length=200, blank=True)
    land = models.CharField(max_length=200, blank=True)
    uid = models.CharField(max_length=20, blank=True, verbose_name="UID")

    def __unicode__(self):
        return self.firma

    class Meta:
        verbose_name = "Firma"
        verbose_name_plural = "Firmen"


class Arbeitnehmer(models.Model):
    """
    Das ORM Model für Arbeitnehmer.
    """
    name = models.CharField(max_length=200, blank=False, unique=True)
    adresse = models.CharField(max_length=200, blank=True)
    plz = models.CharField(max_length=20, blank=True)
    ort = models.CharField(max_length=200, blank=True)
    land = models.CharField(max_length=200, blank=True)
    uid = models.CharField(max_length=20, blank=True, verbose_name="UID")
    bank_name = models.CharField(max_length=200, blank=True,
                                 verbose_name="Bank")
    bank_iban = models.CharField(max_length=200, blank=True,
                                 verbose_name="IBAN")
    bank_bic = models.CharField(max_length=200, blank=True, verbose_name="BIC")

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = "Arbeitnehmer"
        verbose_name_plural = "Arbeitnehmer"


class StundenAufzeichnung(models.Model):
    """
    Das ORM Model für StundenAufzeichnung.
    """
    datum = models.DateField(blank=False)
    firma = models.ForeignKey("Firma")
    startzeit = models.TimeField(blank=False)
    endzeit = models.TimeField(blank=False)
    arbeitnehmer = models.ForeignKey("Arbeitnehmer")
    protokoll = models.TextField(blank=False)
    bezahlt = models.BooleanField(blank=False)

    def stunden(self):
        """
        Rechnet den Zeitunterschied aus.
        """
        return calculate_stunden(self.startzeit, self.endzeit)

    def clean(self):
        """
        Ein Validator, der prüft, ob die Startzeit vor der Endzeit liegt.
        Die geringste Auflösung laut calculate_stunden ist 18 Sekunden.
        """
        if self.startzeit and self.endzeit:
            if float(self.stunden()) <= 0.00:
                raise ValidationError(
                    "Die Endzeit muss nach der Startzeit liegen! "
                    "Außerdem muss der Unterschied mindestens 18 "
                    "Sekunden betragen."
                )

    def __unicode__(self):
        return str(self.firma)

    class Meta:
        verbose_name = "Stunden Aufzeichnung"
        verbose_name_plural = "Stunden Aufzeichnungen"
