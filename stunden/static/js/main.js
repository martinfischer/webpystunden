$(document).ready(function() {

    // Pagination
    function pagination() {
        stundenaufzeichnung = $("#stundenaufzeichnung");
        $(".pages").on("click", function(e){
            var href = $(this).attr("href");
            stundenaufzeichnung.fadeTo("fast", 0.1, function() {
                stundenaufzeichnung.load(href + " #stundenaufzeichnung > *", function () {
                    stundenaufzeichnung.fadeTo("slow", 1);
                    pagination();
                });
            })
            e.preventDefault();
        });
    }
    pagination();

    // Checkboxes toogle all auf der Rechnungsseite
    $(":checkbox#toggle-all").css("visibility", "visible");
    $(":checkbox#toggle-all").click(function() {
        var checkedStatus = this.checked;
        $(":checkbox.toggle-me").each(function() {
            $(this).prop("checked", checkedStatus);
        });
    });

});
