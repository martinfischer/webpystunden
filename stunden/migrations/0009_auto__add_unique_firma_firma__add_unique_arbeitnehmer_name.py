# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding unique constraint on 'Firma', fields ['firma']
        db.create_unique('stunden_firma', ['firma'])

        # Adding unique constraint on 'Arbeitnehmer', fields ['name']
        db.create_unique('stunden_arbeitnehmer', ['name'])


    def backwards(self, orm):
        # Removing unique constraint on 'Arbeitnehmer', fields ['name']
        db.delete_unique('stunden_arbeitnehmer', ['name'])

        # Removing unique constraint on 'Firma', fields ['firma']
        db.delete_unique('stunden_firma', ['firma'])


    models = {
        'stunden.arbeitnehmer': {
            'Meta': {'object_name': 'Arbeitnehmer'},
            'adresse': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'bank_bic': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'bank_iban': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'bank_name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'land': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'}),
            'ort': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'plz': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'uid': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'stunden.firma': {
            'Meta': {'object_name': 'Firma'},
            'adresse': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'firma': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'land': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'ort': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'plz': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'uid': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'stunden.stundenaufzeichnung': {
            'Meta': {'object_name': 'StundenAufzeichnung'},
            'arbeitnehmer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stunden.Arbeitnehmer']"}),
            'bezahlt': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'datum': ('django.db.models.fields.DateField', [], {}),
            'endzeit': ('django.db.models.fields.TimeField', [], {}),
            'firma': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stunden.Firma']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'protokoll': ('django.db.models.fields.TextField', [], {}),
            'startzeit': ('django.db.models.fields.TimeField', [], {})
        }
    }

    complete_apps = ['stunden']