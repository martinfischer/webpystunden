# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'Projekt'
        db.delete_table('stunden_projekt')

        # Adding model 'MeineDaten'
        db.create_table('stunden_meinedaten', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('firma', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('adresse', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('plz', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('ort', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('land', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('uid', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
        ))
        db.send_create_signal('stunden', ['MeineDaten'])

        # Adding model 'Firma'
        db.create_table('stunden_firma', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('firma', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('adresse', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('plz', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('ort', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('land', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('uid', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
        ))
        db.send_create_signal('stunden', ['Firma'])

        # Deleting field 'StundenAufzeichnung.projekt'
        db.delete_column('stunden_stundenaufzeichnung', 'projekt_id')

        # Adding field 'StundenAufzeichnung.firma'
        db.add_column('stunden_stundenaufzeichnung', 'firma',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['stunden.Firma']),
                      keep_default=False)


    def backwards(self, orm):
        # Adding model 'Projekt'
        db.create_table('stunden_projekt', (
            ('projekt', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('plz', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('atu', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('firma', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('ort', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('adresse', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('land', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
        ))
        db.send_create_signal('stunden', ['Projekt'])

        # Deleting model 'MeineDaten'
        db.delete_table('stunden_meinedaten')

        # Deleting model 'Firma'
        db.delete_table('stunden_firma')

        # Adding field 'StundenAufzeichnung.projekt'
        db.add_column('stunden_stundenaufzeichnung', 'projekt',
                      self.gf('django.db.models.fields.related.ForeignKey')(default='Firmenname', to=orm['stunden.Projekt']),
                      keep_default=False)

        # Deleting field 'StundenAufzeichnung.firma'
        db.delete_column('stunden_stundenaufzeichnung', 'firma_id')


    models = {
        'stunden.firma': {
            'Meta': {'object_name': 'Firma'},
            'adresse': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'firma': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'land': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'ort': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'plz': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'uid': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'stunden.meinedaten': {
            'Meta': {'object_name': 'MeineDaten'},
            'adresse': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'firma': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'land': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'ort': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'plz': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'uid': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'stunden.stundenaufzeichnung': {
            'Meta': {'object_name': 'StundenAufzeichnung'},
            'bezahlt': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'datum': ('django.db.models.fields.DateField', [], {}),
            'endzeit': ('django.db.models.fields.TimeField', [], {}),
            'firma': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stunden.Firma']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'protokoll': ('django.db.models.fields.TextField', [], {}),
            'startzeit': ('django.db.models.fields.TimeField', [], {})
        }
    }

    complete_apps = ['stunden']
