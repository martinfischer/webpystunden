# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'MeineDaten.firma'
        db.delete_column('stunden_meinedaten', 'firma')

        # Adding field 'MeineDaten.bank_name'
        db.add_column('stunden_meinedaten', 'bank_name',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True),
                      keep_default=False)

        # Adding field 'MeineDaten.bank_iban'
        db.add_column('stunden_meinedaten', 'bank_iban',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True),
                      keep_default=False)

        # Adding field 'MeineDaten.bank_bic'
        db.add_column('stunden_meinedaten', 'bank_bic',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'MeineDaten.firma'
        db.add_column('stunden_meinedaten', 'firma',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True),
                      keep_default=False)

        # Deleting field 'MeineDaten.bank_name'
        db.delete_column('stunden_meinedaten', 'bank_name')

        # Deleting field 'MeineDaten.bank_iban'
        db.delete_column('stunden_meinedaten', 'bank_iban')

        # Deleting field 'MeineDaten.bank_bic'
        db.delete_column('stunden_meinedaten', 'bank_bic')


    models = {
        'stunden.firma': {
            'Meta': {'object_name': 'Firma'},
            'adresse': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'firma': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'land': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'ort': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'plz': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'uid': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'stunden.meinedaten': {
            'Meta': {'object_name': 'MeineDaten'},
            'adresse': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'bank_bic': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'bank_iban': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'bank_name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'land': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'ort': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'plz': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'uid': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'stunden.stundenaufzeichnung': {
            'Meta': {'object_name': 'StundenAufzeichnung'},
            'bezahlt': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'datum': ('django.db.models.fields.DateField', [], {}),
            'endzeit': ('django.db.models.fields.TimeField', [], {}),
            'firma': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stunden.Firma']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'protokoll': ('django.db.models.fields.TextField', [], {}),
            'startzeit': ('django.db.models.fields.TimeField', [], {})
        }
    }

    complete_apps = ['stunden']