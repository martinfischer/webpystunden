# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'MeineDaten'
        db.delete_table('stunden_meinedaten')

        # Adding model 'Arbeitnehmer'
        db.create_table('stunden_arbeitnehmer', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('adresse', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('plz', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('ort', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('land', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('uid', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('bank_name', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('bank_iban', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('bank_bic', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
        ))
        db.send_create_signal('stunden', ['Arbeitnehmer'])


        # Changing field 'StundenAufzeichnung.arbeitnehmer'
        db.alter_column('stunden_stundenaufzeichnung', 'arbeitnehmer_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stunden.Arbeitnehmer']))

    def backwards(self, orm):
        # Adding model 'MeineDaten'
        db.create_table('stunden_meinedaten', (
            ('bank_name', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('uid', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('bank_iban', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('ort', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('adresse', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('plz', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('land', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('bank_bic', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
        ))
        db.send_create_signal('stunden', ['MeineDaten'])

        # Deleting model 'Arbeitnehmer'
        db.delete_table('stunden_arbeitnehmer')


        # Changing field 'StundenAufzeichnung.arbeitnehmer'
        db.alter_column('stunden_stundenaufzeichnung', 'arbeitnehmer_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stunden.MeineDaten']))

    models = {
        'stunden.arbeitnehmer': {
            'Meta': {'object_name': 'Arbeitnehmer'},
            'adresse': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'bank_bic': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'bank_iban': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'bank_name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'land': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'ort': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'plz': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'uid': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'stunden.firma': {
            'Meta': {'object_name': 'Firma'},
            'adresse': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'firma': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'land': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'ort': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'plz': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'uid': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'stunden.stundenaufzeichnung': {
            'Meta': {'object_name': 'StundenAufzeichnung'},
            'arbeitnehmer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stunden.Arbeitnehmer']"}),
            'bezahlt': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'datum': ('django.db.models.fields.DateField', [], {}),
            'endzeit': ('django.db.models.fields.TimeField', [], {}),
            'firma': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stunden.Firma']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'protokoll': ('django.db.models.fields.TextField', [], {}),
            'startzeit': ('django.db.models.fields.TimeField', [], {})
        }
    }

    complete_apps = ['stunden']