# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Projekt.firma'
        db.add_column('stunden_projekt', 'firma',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True),
                      keep_default=False)

        # Adding field 'Projekt.name'
        db.add_column('stunden_projekt', 'name',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Projekt.firma'
        db.delete_column('stunden_projekt', 'firma')

        # Deleting field 'Projekt.name'
        db.delete_column('stunden_projekt', 'name')


    models = {
        'stunden.projekt': {
            'Meta': {'object_name': 'Projekt'},
            'adresse': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'atu': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'firma': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'land': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'ort': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'plz': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'projekt': ('django.db.models.fields.CharField', [], {'max_length': '500'})
        },
        'stunden.stundenaufzeichnung': {
            'Meta': {'object_name': 'StundenAufzeichnung'},
            'bezahlt': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'datum': ('django.db.models.fields.DateField', [], {}),
            'endzeit': ('django.db.models.fields.TimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'projekt': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stunden.Projekt']"}),
            'protokoll': ('django.db.models.fields.TextField', [], {}),
            'startzeit': ('django.db.models.fields.TimeField', [], {})
        }
    }

    complete_apps = ['stunden']