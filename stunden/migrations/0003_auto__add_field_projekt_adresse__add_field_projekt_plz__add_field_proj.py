# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Projekt.adresse'
        db.add_column('stunden_projekt', 'adresse',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True),
                      keep_default=False)

        # Adding field 'Projekt.plz'
        db.add_column('stunden_projekt', 'plz',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=20, blank=True),
                      keep_default=False)

        # Adding field 'Projekt.ort'
        db.add_column('stunden_projekt', 'ort',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True),
                      keep_default=False)

        # Adding field 'Projekt.land'
        db.add_column('stunden_projekt', 'land',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True),
                      keep_default=False)

        # Adding field 'Projekt.atu'
        db.add_column('stunden_projekt', 'atu',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=20, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Projekt.adresse'
        db.delete_column('stunden_projekt', 'adresse')

        # Deleting field 'Projekt.plz'
        db.delete_column('stunden_projekt', 'plz')

        # Deleting field 'Projekt.ort'
        db.delete_column('stunden_projekt', 'ort')

        # Deleting field 'Projekt.land'
        db.delete_column('stunden_projekt', 'land')

        # Deleting field 'Projekt.atu'
        db.delete_column('stunden_projekt', 'atu')


    models = {
        'stunden.projekt': {
            'Meta': {'object_name': 'Projekt'},
            'adresse': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'atu': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'land': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'ort': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'plz': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'projekt': ('django.db.models.fields.CharField', [], {'max_length': '500'})
        },
        'stunden.stundenaufzeichnung': {
            'Meta': {'object_name': 'StundenAufzeichnung'},
            'bezahlt': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'datum': ('django.db.models.fields.DateField', [], {}),
            'endzeit': ('django.db.models.fields.TimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'projekt': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stunden.Projekt']"}),
            'protokoll': ('django.db.models.fields.TextField', [], {}),
            'startzeit': ('django.db.models.fields.TimeField', [], {})
        }
    }

    complete_apps = ['stunden']