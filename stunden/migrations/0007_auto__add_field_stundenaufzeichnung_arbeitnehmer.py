# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'StundenAufzeichnung.arbeitnehmer'
        db.add_column('stunden_stundenaufzeichnung', 'arbeitnehmer',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['stunden.MeineDaten']),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'StundenAufzeichnung.arbeitnehmer'
        db.delete_column('stunden_stundenaufzeichnung', 'arbeitnehmer_id')


    models = {
        'stunden.firma': {
            'Meta': {'object_name': 'Firma'},
            'adresse': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'firma': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'land': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'ort': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'plz': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'uid': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'stunden.meinedaten': {
            'Meta': {'object_name': 'MeineDaten'},
            'adresse': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'bank_bic': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'bank_iban': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'bank_name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'land': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'ort': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'plz': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'uid': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'stunden.stundenaufzeichnung': {
            'Meta': {'object_name': 'StundenAufzeichnung'},
            'arbeitnehmer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stunden.MeineDaten']"}),
            'bezahlt': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'datum': ('django.db.models.fields.DateField', [], {}),
            'endzeit': ('django.db.models.fields.TimeField', [], {}),
            'firma': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stunden.Firma']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'protokoll': ('django.db.models.fields.TextField', [], {}),
            'startzeit': ('django.db.models.fields.TimeField', [], {})
        }
    }

    complete_apps = ['stunden']