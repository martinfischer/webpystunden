# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Projekt'
        db.create_table('stunden_projekt', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('projekt', self.gf('django.db.models.fields.CharField')(max_length=500)),
        ))
        db.send_create_signal('stunden', ['Projekt'])

        # Adding model 'StundenTabelle'
        db.create_table('stunden_stundentabelle', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('datum', self.gf('django.db.models.fields.DateField')()),
            ('projekt', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stunden.Projekt'])),
            ('startzeit', self.gf('django.db.models.fields.TimeField')()),
            ('endzeit', self.gf('django.db.models.fields.TimeField')()),
            ('protokoll', self.gf('django.db.models.fields.TextField')()),
            ('bezahlt', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('stunden', ['StundenTabelle'])


    def backwards(self, orm):
        # Deleting model 'Projekt'
        db.delete_table('stunden_projekt')

        # Deleting model 'StundenTabelle'
        db.delete_table('stunden_stundentabelle')


    models = {
        'stunden.projekt': {
            'Meta': {'object_name': 'Projekt'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'projekt': ('django.db.models.fields.CharField', [], {'max_length': '500'})
        },
        'stunden.stundentabelle': {
            'Meta': {'object_name': 'StundenTabelle'},
            'bezahlt': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'datum': ('django.db.models.fields.DateField', [], {}),
            'endzeit': ('django.db.models.fields.TimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'projekt': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stunden.Projekt']"}),
            'protokoll': ('django.db.models.fields.TextField', [], {}),
            'startzeit': ('django.db.models.fields.TimeField', [], {})
        }
    }

    complete_apps = ['stunden']