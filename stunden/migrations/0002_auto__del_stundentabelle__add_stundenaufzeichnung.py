# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'StundenTabelle'
        db.delete_table('stunden_stundentabelle')

        # Adding model 'StundenAufzeichnung'
        db.create_table('stunden_stundenaufzeichnung', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('datum', self.gf('django.db.models.fields.DateField')()),
            ('projekt', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stunden.Projekt'])),
            ('startzeit', self.gf('django.db.models.fields.TimeField')()),
            ('endzeit', self.gf('django.db.models.fields.TimeField')()),
            ('protokoll', self.gf('django.db.models.fields.TextField')()),
            ('bezahlt', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('stunden', ['StundenAufzeichnung'])


    def backwards(self, orm):
        # Adding model 'StundenTabelle'
        db.create_table('stunden_stundentabelle', (
            ('endzeit', self.gf('django.db.models.fields.TimeField')()),
            ('bezahlt', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('projekt', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stunden.Projekt'])),
            ('protokoll', self.gf('django.db.models.fields.TextField')()),
            ('datum', self.gf('django.db.models.fields.DateField')()),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('startzeit', self.gf('django.db.models.fields.TimeField')()),
        ))
        db.send_create_signal('stunden', ['StundenTabelle'])

        # Deleting model 'StundenAufzeichnung'
        db.delete_table('stunden_stundenaufzeichnung')


    models = {
        'stunden.projekt': {
            'Meta': {'object_name': 'Projekt'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'projekt': ('django.db.models.fields.CharField', [], {'max_length': '500'})
        },
        'stunden.stundenaufzeichnung': {
            'Meta': {'object_name': 'StundenAufzeichnung'},
            'bezahlt': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'datum': ('django.db.models.fields.DateField', [], {}),
            'endzeit': ('django.db.models.fields.TimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'projekt': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['stunden.Projekt']"}),
            'protokoll': ('django.db.models.fields.TextField', [], {}),
            'startzeit': ('django.db.models.fields.TimeField', [], {})
        }
    }

    complete_apps = ['stunden']